﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace PureMVC.Unity
{
    /// <summary>
    /// Базовый класс для всех скриптов типа Вид, подключаемых к объектам на сцене 
    /// </summary>
    public abstract class CustomView : CustomBehavior
    {

    }
}
