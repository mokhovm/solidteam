﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PureMVC.Interfaces;
using UnityEngine;

namespace PureMVC.Unity
{
    /*
     *
     *  Нотификатор, унаследованный от MonoBehaviour
     *  Копирует класс Notifier и используется для реализации MediatorBehavior
     * 
     */

    public class NotifierBehavior : CustomBehavior, INotifier
    {
        //private IFacade m_facade = PureMVC.Patterns.Facade.Instance 

        public void SendNotification(string notificationName)
        {
            PureMVC.Patterns.Facade.Instance.SendNotification(notificationName);
        }

        public void SendNotification(string notificationName, object body)
        {
            PureMVC.Patterns.Facade.Instance.SendNotification(notificationName, body);
        }

        public void SendNotification(string notificationName, object body, string type)
        {
            PureMVC.Patterns.Facade.Instance.SendNotification(notificationName, body, type);
        }

        protected IFacade Facade
        {
            get
            {
                //if (m_facade == null) m_facade = PureMVC.Patterns.Facade.Instance; 
                return PureMVC.Patterns.Facade.Instance;
            }
        }
    }
}
