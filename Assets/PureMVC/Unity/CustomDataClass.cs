﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PureMVC.Unity
{
    /// <summary>
    /// Базовый класс для создания элементов модели
    /// </summary>
    public abstract class CustomDataClass
    {
        public abstract void update();
    }
}
