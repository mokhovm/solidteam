using System.Runtime.CompilerServices;
//using Boo.Lang;
using Common.Units.fsm;
using PureMVC.Patterns;
using SampleGameNamespace;
using UnityEngine;

namespace Common.Units
{
  
    
    /// <summary>
    /// Базовый класс для всех интерактивных объектов.
    /// Мы исходим из того, что каждый интерактивный юнит самодостаточен и умеет работать с собственными состояниями
    /// Одной из базовых настроек юнита является имя префаба, который содержит всю необходимую визуальную и
    /// аудиосоставляющую, созданную по солгашениям и управляемую медиатором, который получает сообщения от
    /// интерактивного юнита. 
    /// 
    /// </summary>
    public class InteractiveUnit : SerializeObject
    {
        // ссылка на аватар юнита
        public GameObject Avatar { get; set; }

        /// <summary>
        /// состояние юнита
        /// </summary>
        public UnitState State
        {
            get
            {
               return StateMachine.CurrentState;
            }
        }

        /// <summary>
        /// Встроенная в каждый юнит машина состояний
        /// </summary>
        public UnitStateMachine StateMachine;
        
        /// <summary>
        /// Текущие характеристики юнита
        /// </summary>
        public Fact Params;
        
        /// <summary>
        /// начальные настройки юнита из БД
        /// </summary>
        public Fact InitialSettings;

        /// <summary>
        /// Активен ли юнит
        /// </summary>
        public bool IsActive;
        //public bool IsStopAnimation;
        
        /// <summary>
        /// Данный конструктор создает экземпляр класса и инициализирует машину состояний 
        /// </summary>
        /// <param name="initialSettings"></param>
        /// <param name="avatar"></param>
        public InteractiveUnit(Fact initialSettings, GameObject avatar) : base()
        {
            Avatar = avatar;
            StateMachine = new UnitStateMachine(this);
            InitialSettings = Clone(initialSettings);
            StateMachine.Init(InitialSettings.States);
        }

        /// <summary>
        /// Первое состояние машины. Относится к числу обязательных
        /// </summary>
        public virtual void OnStateCreate( object[] parameters)
        {
            Debug.Log("Execute OnStateCreate");
        }
    
        /// <summary>
        /// Второе состояние машины. Относится к числу обязательных
        /// </summary>
        public virtual void OnStateInit( object[] parameters)
        {
            Debug.Log("Execute OnStateInit");
            Params = Clone(InitialSettings);
            StateMachine.ChangeState("disabled", null);
        }
        
        /// <summary>
        /// Факультативное состояние ожидания. Рекомендуется к реализации
        /// </summary>
        public virtual void OnStateIdle( object[] parameters)
        {
            Debug.Log("Execute OnStateIdle");   
        }

        /// <summary>
        /// Факультативное состояние смерти объекта. Рекомендуется к реализации
        /// </summary>
        public virtual void OnStateDead( object[] parameters)
        {
            Debug.Log("Execute OnStateDead");   
        }
        
        /// <summary>
        /// Состояние освобождения ресурсов. Относится к числу обязательных
        /// </summary>
        public virtual void OnStateFree( object[] parameters)
        {
            Debug.Log("Execute OnStateFree");   
            StateMachine = null;
            Avatar = null;
            InitialSettings = null;
            Params = null;
        }

        /// <summary>
        /// Факультативное состояние для реализации логици в потомках без изменения конфига машины состояний
        /// </summary>
        public virtual void OnOtherState(object[] parameters)
        {
            Debug.Log("Execute OnOtherState " + State.ToString());
            switch (State.Name)
            {
                case "disabled":
                    Facade.Instance.SendNotification(BzMessages.LAMP_SWITCH, false);
                    break;
                case "enabled":
                    Facade.Instance.SendNotification(BzMessages.LAMP_SWITCH, true);
                    break;
            }
        }

        public bool OnCreateEntering(object[] parameters)
        {
            return true;
        }
        
        public bool OnCreateExiting(object[] parameters)
        {
            return true;
        }
        

        /// <summary>
        /// Осуществляем управление юнитом
        /// Этот метод должен вызываться из Update
        /// </summary>
        public void calcaulate()
        {
            ///Debug.Log("calculate");
            
        }

        /// <summary>
        /// Расчитываем поведение персонажа в физическом мире. 
        /// Этот метод должен вызываться из FixedUpdate
        /// </summary>
        public void calculatePhysics()
        {
            //Debug.Log("calculatePhysics");
        }


        public void changeLight()
        {
            Debug.Log("changeLight");
            switch (State.Name)
            {
                case "disabled":
                    StateMachine.ChangeState("enabled", null);
                    break;
                case "enabled":
                    StateMachine.ChangeState("disabled", null);
                    break;
            }
        }
    }
}