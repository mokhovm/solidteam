using Newtonsoft.Json;
using UnityEngine;
using Object = System.Object;

namespace Common.Units
{
    public class SerializeObject
    {
        /// <summary>
        /// Копирует объект с помощью Json сериализации. Не работает с приватными членами
        /// Использует библиотеку https://www.newtonsoft.com/json
        /// Но для работой с Юниити удобнее использовать адаптацию для Unity3D https://github.com/SaladLab/Json.Net.Unity3D/releases
        /// </summary>
        /// <typeparam name="T">Тип объекта копированя.</typeparam>
        /// <param name="source">Экземпляр объекта</param>
        /// <returns>Копию объекта</returns>
        public static T Clone<T>(T source)
        {            
            // Don't serialize a null object, simply return the default for that object
            if (Object.ReferenceEquals(source, null))
            {
                return default(T);
            }
            
            /*

            // initialize inner objects individually
            // for example in default constructor some list property initialized with some values,
            // but in 'source' these items are cleaned -
            // without ObjectCreationHandling.Replace default constructor values will be added to result
            var settings = new JsonSerializerSettings {ObjectCreationHandling = ObjectCreationHandling.Replace};

            return JsonConvert.DeserializeObject<T>(JsonConvert.SerializeObject(source), settings);
            */
            
            // переписал на бибилиотеку из unity, так как она корректно сериализирует Vector3
            // JsonConvert отказывается это делать
            string json = Serialize(source);
            return Deserialize<T>(json);
        }

        /// <summary>
        /// Сериализирует объект в JSON строку
        /// </summary>
        /// <param name="source">объект для сериализации</param>
        /// <returns></returns>
        public static string Serialize(object source)
        {
            //return JsonConvert.SerializeObject(source);
            return JsonUtility.ToJson(source, true);
        }
        
        

        /// <summary>
        /// Сериализирует объект в JSON строку
        /// </summary>
        /// <param name="json">сериализиованный объект для десериализации</param>
        /// <returns></returns>
        public static T Deserialize<T>(string json)
        {
            //return JsonConvert.SerializeObject(source);
            return JsonUtility.FromJson<T>(json);
        }        
    }
}