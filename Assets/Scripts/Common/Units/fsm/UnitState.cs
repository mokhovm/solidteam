using System;
using System.Collections.Generic;

namespace Common.Units.fsm
{
    /// <summary>
    /// состояние юнита
    /// </summary>
    public class UnitState
    {
        /// <summary>
        ///  Имя состояния
        /// </summary>
        public string Name;
		
        /// <summary>
        /// Название метода юнита, который вызывается перед входом в состояние.
        /// Если вернет false, то переход отменяется
        /// </summary>
        public string Entering;
		
        /// <summary>
        /// Название метода юнита, который вызывается перед выходом из состояния
        /// Если вернет false, то переход отменяется
        /// </summary>
        public string Exiting;

        /// <summary>
        /// Название метода юнита, который вызывается при переходе в состояние
        /// </summary>
        public string Changed;
        
        /// <summary>
        /// Признак начального состояния
        /// </summary>
        public bool IsInitial = false;
        
        /**
         *  Регистрирует список переходов из этого состояния
         */ 
        protected Dictionary<string, string> Transitions = new Dictionary<string, string>();
        

        /**
         * Constructor.
         * 
         * @param id the id of the state
         * @param entering an optional notification name to be sent when entering this state
         * @param exiting an optional notification name to be sent when exiting this state
         * @param changed an optional notification name to be sent when fully transitioned to this state
         */
        public UnitState( string name, string entering = null, string exiting = null, string changed = null )
        {
            Name = name;
            if ( !string.IsNullOrEmpty(entering)) Entering = entering;
            if ( !string.IsNullOrEmpty(exiting)) Exiting  = exiting;
            if ( !string.IsNullOrEmpty(changed)) Changed = changed;
        }
	
        /** 
         * Define a transition. 
         * 
         * @param action the name of the StateMachine.ACTION Notification type.
         * @param target the name of the target state to transition to.
         */
        public void DefineTrans( string action, string target)
        {
            if ( GetTarget( action ) == null ) 	
                Transitions[ action ] = target;
            else
                throw new Exception("action " + action + " is already exists");
        }

        /** 
         * Remove a previously defined transition.
         */
        public void RemoveTrans( string action)
        {
            Transitions[ action ] = null;	
        }	
		
        /**
         * Get the target state name for a given action.
         */
        public string GetTarget( string action )
        {
            string res;
            if (Transitions.ContainsKey(action))
                res = Transitions[action];
            else
                res = null;
            return res;
        }

        public int GetTransitionQty()
        {
            return Transitions.Count;
        }

        public override string ToString()
        {
            return Name;
        }
    }
}