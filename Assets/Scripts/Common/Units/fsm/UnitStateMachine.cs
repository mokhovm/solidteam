using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Newtonsoft.Json.Linq;
using UnityEngine;
using UnityEngine.XR.WSA.Sharing;

namespace Common.Units.fsm
{
    
    /// <summary>
    /// Машина состояний для юнита
    /// </summary>
    public class UnitStateMachine
    {
	    public InteractiveUnit Unit { get; set; }

	    /// <summary>
	    /// Список поддерживаемых состояний
	    /// </summary>
	    private readonly Dictionary<string, UnitState> m_states = new Dictionary<string, UnitState>();

	    /// <summary>
	    /// Начальное состояние машины
	    /// </summary>
	    protected UnitState Initial;
		

	    private UnitState m_prevState;
	    private UnitState m_state;

	    /// <summary>
	    /// текущее состояние
	    /// </summary>
	    public UnitState CurrentState
	    {
		    get { return m_state; }
		    set
		    {
			    PrevState = m_state;
			    m_state = value;
		    }
	    }

	    public UnitState PrevState
	    {
		    get { return m_prevState; }
		    set { m_prevState = value; }
	    }

	    public UnitStateMachine(InteractiveUnit unit)
	    {
		    Unit = unit;
	    }
		
	    public void Init(string info)
	    {
		    InjectStates(info);
			if ( Initial != null ) TransitionTo( Initial );	
		}

	    /// <summary>
	    /// Инициализирует машину данными в тестовом формате
	    /// </summary>
	    /// <param name="info">описание машины состояний</param>
	    /// <exception cref="NotImplementedException"></exception>
	    private void InjectStates(string info)
	    {
		    // 1. получить json, описывающий машину состояний
		    // 2. распарсить json
		    // 3. создать состояния
		    // 4. проверить работоспособность переходов
		    
		    // решение
		    // 1. костыль на время
		    info = System.IO.File.ReadAllText(@"Assets\Scripts\GameParts\BattleZone\Proxies\states.json");
		    // 2, 3
		    
		    JObject fsm = JObject.Parse(info);

		    // get JSON result objects into a list
		    IList<JToken> states = fsm["states"].Children().ToList();
		    Debug.Log("found " + states.Count + " states");

		    // serialize JSON results into .NET objects
		    foreach (JToken value in states)
		    {
			    Debug.Log("get new json state " + value.ToString());
			    // JToken.ToObject is a helper method that uses JsonSerializer internally
			    UnitState newState = value.ToObject<UnitState>();
			    Debug.Log("newState " + newState.Name + " deserialized complete");
			    IList<JToken> transitions = value["Transitions"].Children().ToList();
			    foreach (JToken tra in transitions)
			    {
				    newState.DefineTrans(tra["action"].ToString(), tra["target"].ToString());
			    }
			    Debug.Log(newState.Name + " transitions qty=" + newState.GetTransitionQty() );
			    RegisterState(newState);
		    }
	    }

	    /// <summary>
	    /// Регистрирует состояние в машине
	    /// </summary>
	    /// <param name="state">инстанциированное состояние</param>
	    /// <param name="initial">признак того, что оно стартовое</param>
		public void RegisterState( UnitState state)
		{
			// ReSharper disable once InvertIf
			if (state != null && !m_states.ContainsKey(state.Name))
			{
				m_states[ state.Name ] = state;
				if ( state.IsInitial) Initial = state; 
			}
		}
	    
		/**
		 * Remove a state mapping. 
		 * <P>
		 * Removes the entry and exit commands for a given state 
		 * as well as the state mapping itself.</P>
		 * 
		 * @param state
		 */
		public void RemoveState( string stateName)
		{
			var state = m_states[ stateName ];
			if ( state == null ) return;
			m_states[ stateName ] = null;
		}
		

	    /// <summary>
	    ///  Осуществляет переход в другое состояние
	    /// </summary>
	    /// <param name="nextState"> новое состояние</param>
	    /// <param name="data">какие-то параметры, которые будут проброшены в методы, обрабатываеющие переход</param>
	    /// <returns></returns>
	    private bool TransitionTo( UnitState nextState, object data = null )
		{
			var res = false;
			
			// ReSharper disable once InvertIf
			if (nextState != null )
			{
				res = CurrentState == null || CurrentState.Exiting == null || 
				      CanTransition(CurrentState.Exiting, new[] {data});

				if (res && nextState.Entering != null)
				{
					res = CanTransition(nextState.Entering, new[] {data});
				}

				// ReSharper disable once InvertIf
				// Если оба гарда подтвердили переход, то перейдем в новое состояние
				if (res)
				{
					// change the current state only when both guards have been passed
					CurrentState = nextState;
					if (CurrentState.Changed != null)
						ExecuteMethod(CurrentState.Changed, new[] {data});
				}
			}
			return res;
		}

	    /// <summary>
	    /// Вызывает метод инетрактивного юнита с параметрами
	    /// </summary>
	    /// <param name="methodName"> имя метода</param>
	    /// <param name="parameters"> массив параметров</param>
	    /// <returns> вернет объект</returns>
	    private object ExecuteMethod(string methodName, object[] parameters)
	    {
		    object res;
		    if (Unit != null)
		    {
			    var unitType = Unit.GetType();
			    var theMethod = unitType.GetMethod(methodName);
			    if (theMethod == null)
				    throw new Exception("Method " + methodName + " in the class " + 
						unitType.ToString() + " is not found");
			    res = theMethod.Invoke(Unit, parameters);
		    }
		    else
		    {
			    res = null;
		    }
		    return res;
	    }

	    /// <summary>
	    /// Вызывает метод интерактивного юнита для проверки выхода из текущего состояния или входа в новое
	    /// </summary>
	    /// <param name="methodName"> имя проверяющего метода</param>
	    /// <param name="parameters"> массив параметров</param>
	    /// <returns> вернет тру, если проверяющий метод не против перехода</returns>
	    private bool CanTransition(string methodName, object[] parameters)
	    {
		    bool res;
		    var obj = ExecuteMethod(methodName, parameters);
		    res = (obj is bool) && (bool) obj;
		    return res;
	    }
	    
	    
			
	    /// <summary>
	    /// метод предназначен для перехода к новому состоянию 
	    /// </summary>
	    /// <param name="stateName">имя нового состояния</param>
	    /// <param name="data">какие-то параметры, которые будут проброжены в методы, обрабатываеющие переход</param>
	    public bool ChangeState(string stateName, object data)
	    {
		    bool res;
		    /*
		    var target = CurrentState.GetTarget(stateName);
		    var newState = m_states[ target ];
		    */
		    var newState = m_states[ stateName ];
		    res = newState != null && TransitionTo(newState, data);
		    return res;
	    }
	    
    }
}