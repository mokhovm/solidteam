namespace Common.Units
{
    /// <summary>
    /// Класс, описывающий базовые данные юнита
    /// Это могут быть бафы, дебаффы,
    /// </summary>
    public class BaseFact : SerializeObject
    {
        /// <summary>
        /// Идентификатор модификатора (как правило ид записи в БД)
        /// </summary>
        public int Id;
        
        /// <summary>
        /// Название модификатора
        /// </summary>
        public string Caption;
        
        /// <summary>
        /// Описание модификатора
        /// </summary>
        public string Description;
        
        /// <summary>
        /// Имя класса для инстанциации
        /// </summary>
        public string ClassName;
        
        /// <summary>
        /// влключен ли
        /// </summary>
        public bool IsEnable;
        
        /// <summary>
        /// Имя префаба, представляющего юнит на сцене
        /// </summary>
        public string PrefabName;
        
        /// <summary>
        /// Описание машины состояний 
        /// </summary>
        public string States;
        
        /// <summary>
        /// прочие неструктурированные параметры в JSON 
        /// </summary>
        public string SomeParams;
        
    }
}