using UnityEngine;

namespace Common.Units
{
    /// <inheritdoc />
    /// <summary>
    /// Оружие
    /// </summary>
    public class Weapon : BaseFact
    {
        /// <summary>
        /// Минимальные повреждения от применения
        /// </summary>
        public float MinDamage;
        
        /// <summary>
        /// Максимальные повреждения от применения
        /// </summary>
        public float MaxDamage;
        
        /// <summary>
        /// Частота применения в секунду
        /// </summary>
        public float UseRate;
        
        /// <summary>
        /// Минимальная дальность применения
        /// </summary>
        public float MinRange;
        
        /// <summary>
        ///  Максимальнная дальность применения
        /// </summary>
        public float MaxRange;
        
        /// <summary>
        /// Емкость магазина
        /// </summary>
        public float Capacity;
        
        /// <summary>
        /// Задержка при переключении на это оружие в сек 
        /// </summary>
        protected float ChangeDelay;
        
        /// <summary>
        /// Задержка при перезарядке на это оружие в сек 
        /// </summary>
        protected float ReloadDelay;
        
        /// <summary>
        /// Скорость полета пули
        /// </summary>
        public float BulletSpeed;
        
        /// <summary>
        /// Разброс пуль
        /// </summary>
        public Vector2 BulletSpread;
        
        /// <summary>
        /// Сила воздействия пули на предмет, с которым она столкнулась
        /// </summary>
        public float ImpactForce;

        /// <summary>
        /// идентификатор юнита, описывающего пулю для этого оружия
        /// </summary>
        public int BulletId;

        public Weapon()
        {
            
        }

    }
}