using System.Collections.Generic;
using UnityEngine;

namespace Common.Units
{
    
    /// <summary>
    /// Тип защиты юнита
    /// </summary>
    public enum UnitProtectionType
    {
        ptNone = 0,
        ptNormal
    }
    
    /// <summary>
    ///  Направление взгляда юнита
    /// </summary>
    public enum UnitOrientation
    {
        uoLeft = -1,
        uoRight = 1
    }
    
    /// <inheritdoc />
    /// <summary>
    /// Базовые параметры юнита
    /// </summary>
    public class Fact : BaseFact
    {
        /// <summary>
        ///  линейная скорость
        /// </summary>
        public float LineSpeed;
        
        /// <summary>
        /// Скорость поворота
        /// </summary>
        public float RotationSpeed;
        
        /// <summary>
        /// Ускорение юнита
        /// </summary>
        public float AccelerationSpeed;
        
        /// <summary>
        /// Скорость прыжка
        /// </summary>
        public float JumpSpeed;
        
        /// <summary>
        /// Высота прыжка
        /// </summary>
        public float JumpHeight;
        
        /// <summary>
        /// Скорость приседания
        /// </summary>
        public float CrouchSpeed;
        
        /// <summary>
        /// Масса юнита
        /// </summary>
        public float Mass;
        
        /// <summary>
        /// Максимальное здоровье юнита
        /// </summary>
        public float MaxHealth;
        
        /// <summary>
        /// Текущее здоровье юнита
        /// </summary>
        public float Health;
        
        /// <summary>
        /// Величина зашиты
        /// </summary>
        public float ProtectionValue;
        
        /// <summary>
        /// тип защиты
        /// </summary>
        public UnitProtectionType ProtectionType;
        
        /// <summary>
        /// направление взгляда юнита
        /// </summary>
        public UnitOrientation Orientation;

        /// <summary>
        /// положение юнита
        /// </summary>
        public Vector3 Position;
        
        /// <summary>
        /// Список оружия у юнита
        /// </summary>
        public List<Weapon> WeaponList;
        
        /// <summary>
        /// Текущие модификаторы: баффы, дебаффы, особенности (например, дроп)
        /// </summary>
        public List<BaseFact> ModifierList;
        
        /// неподдерживаемые юнитом модификаторы и оружие
        /// Это могут быть особенности юнита, его невосприимчивость к определенным типам баффов и дебаффов
        public List<BaseFact> NonAcceptableModifierList;

        public Fact()
        {
            WeaponList = new List<Weapon>();
            ModifierList = new List<BaseFact>();
            NonAcceptableModifierList = new List<BaseFact>();
        }
    }
}