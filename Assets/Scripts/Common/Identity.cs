using UnityEngine;

namespace Common
{
    // для примера идентификации объектов
    
    // заранее заданные типы объектов 
    public enum IdentityType
    {
        itObstacle,
        itBullet,
        itEnemy
    }
    
    // скрипт, который надо положить на GO, чтобы указать у GO тип объекта
    // позднее можно будет найти по компомненту и определить этот тип.
    public class Identity : MonoBehaviour
    {
        public IdentityType type;
    }
}