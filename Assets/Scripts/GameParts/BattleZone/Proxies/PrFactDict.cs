using System.Collections.Generic;
using Common.Units;
using PureMVC.Patterns;
using UnityEngine;

namespace SampleGameNamespace
{
    /// <summary>
    /// Прокси для справочника юнитов
    /// </summary>
    public class PrFactDict : Proxy
    {
        public new const string NAME = "PrFactDict";
        
        public List<BaseFact> Facts = new List<BaseFact>(); 

        public PrFactDict() : base (NAME, null)
        {
            Debug.Log(NAME + " started");
            Facts.Add(
                new Fact() {Caption =  "Лампа", Description = "Подвесная лампа", ClassName = "Common.Units.InteractiveUnit", 
                    Id = 1, IsEnable = true, PrefabName = "", SomeParams = "{}", States = "{}"});
        }

        public override void OnRegister()
        {
            base.OnRegister();
        }

        public override void OnRemove()
        {
            base.OnRemove();
            //Facts.Clear();
        }
    }
}