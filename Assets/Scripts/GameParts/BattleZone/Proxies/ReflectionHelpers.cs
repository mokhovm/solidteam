using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;

public static class ReflectionHelpers
{
    public static System.Type[] GetAllDerivedTypes(this System.AppDomain aAppDomain, System.Type aType)
    {
        var result = new List<System.Type>();
        var assemblies = aAppDomain.GetAssemblies();
        foreach (var assembly in assemblies)
        {
            var types = assembly.GetTypes();
            foreach (var type in types)
            {
                if (type.IsSubclassOf(aType))
                    result.Add(type);
            }
        }
        return result.ToArray();
    }

    public static System.Type[] GetAllTypes(this System.AppDomain aAppDomain, string TypeName)
    {
        var res = new List<System.Type>();
        System.Type[] typesInThisAssembly = Assembly.GetExecutingAssembly().GetTypes();
        foreach (System.Type type in typesInThisAssembly)
        {
            if (type.FullName == TypeName)
            {
                res.Add(type);
            }
        }
        return res.ToArray();
    }

}