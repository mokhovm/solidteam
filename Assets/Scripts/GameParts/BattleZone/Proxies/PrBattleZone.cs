﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using Common.Units;
using PureMVC.Patterns;
using UnityEngine;

namespace SampleGameNamespace
{

    public enum ZoneState
    {
        zsUnknown = 0,
        zsInit,
        zsInProgress,
        zsPause,
        zsSettings,
        zsEnd
    }

    /// <summary>
    /// Управляет моделью игровой зоны
    /// </summary>
    class PrBattleZone : Proxy
    {
        public new const string NAME = "PrBattleZone";

        // состояние игровой зоны (см. BzMessages.BATTLE_ZONE_STATE)
        private ZoneState _state = ZoneState.zsUnknown;
        public ZoneState state {
            get { return _state; }
            set { if (_state != value) changeState(value); }
        }

        public Hero hero;
        public PrFactDict PrFactDict;
        
        // список интерактивных предметов в зоне
        public List<InteractiveUnit> units = new List<InteractiveUnit>();

        public override void OnRegister()
        {
            base.OnRegister();
            PrFactDict = Facade.RetrieveProxy(PrFactDict.NAME) as PrFactDict;
            
        }

        public override void OnRemove()
        {
            base.OnRemove();
            PrFactDict = null;
        }

        private void changeState(ZoneState value)
        {
            _state = value;
            SendNotification(BzMessages.STATE_WAS_CHANGED, _state);
        }

        public PrBattleZone() : base (NAME, null)
        {
            Debug.Log(NAME + " started");
            hero = new Hero();
        }

        public ZoneState messageToState(string note)
        {
            ZoneState res;
            switch (note)
            {
                // идет инициализация
                case BzMessages.STATE_INIT:
                    res = ZoneState.zsInit;
                    break;
                // идет игра
                case BzMessages.STATE_IN_PROGRESS:
                    res = ZoneState.zsInProgress;
                    break;
                // игра остановлена
                case BzMessages.STATE_PAUSE:
                    res = ZoneState.zsPause;
                    break;
                // открыты настройки игры
                case BzMessages.STATE_SETTINGS:
                    res = ZoneState.zsSettings;
                    break;
                // игра окончена
                case BzMessages.STATE_END:
                    res = ZoneState.zsEnd;
                    break;
                default:
                    res = ZoneState.zsUnknown;
                    break;
            }
            return res;
        }

        public void setStateByMessage(string message)
        {
            state = messageToState(message);
        }
        
        public static System.Type[] GetAllDerivedTypes(System.AppDomain aAppDomain, System.Type aType)
        {
            var result = new List<System.Type>();
            var assemblies = aAppDomain.GetAssemblies();
            foreach (var assembly in assemblies)
            {
                var types = assembly.GetTypes();
                foreach (var type in types)
                {
                    if (type.IsSubclassOf(aType))
                        result.Add(type);
                }
            }
            return result.ToArray();
        }
        
        
        
        public object GetInstance(string FullyQualifiedNameOfClass, object[] args)
        {
            Debug.Log("class =" + FullyQualifiedNameOfClass + " count=" + args.Length );
            Type type = Type.GetType(FullyQualifiedNameOfClass);
            if (type != null)
                return Activator.CreateInstance(type, args);
            
            
            foreach (var asm in AppDomain.CurrentDomain.GetAssemblies())
            {
                type = asm.GetType(FullyQualifiedNameOfClass);
                if (type != null)
                    return Activator.CreateInstance(type, args);
            }
            return null;
        }
        
        
        public object GetInstance2(string FullyQualifiedNameOfClass, object[] args)
        {
            object res = null;
            System.Type[] typesInThisAssembly = Assembly.GetExecutingAssembly().GetTypes();
            foreach (System.Type type in typesInThisAssembly)
            {
                if (type.ToString() == FullyQualifiedNameOfClass)
                {
                    res = Activator.CreateInstance(type, args);
                }
            }
            return res;
        }

        public void ShowTypes(string className)
        {
            //var types = System.AppDomain.CurrentDomain.GetAllDerivedTypes(typeof(InteractiveUnit));
            var types = System.AppDomain.CurrentDomain.GetAllTypes(className);
            Debug.Log(types.Length);
            foreach (var type in types)
            {
                Debug.Log(type.FullName);
            }
        }

        /*
         * Создает юнит по данным из справочника фактов
         */
        public InteractiveUnit CreateUnit(int factId, GameObject avatar)
        {
            InteractiveUnit res = null;
            if (PrFactDict == null) Debug.Log("PrFactDict is null");
            Fact fact = PrFactDict.Facts.Find(x => x.Id == factId) as Fact;
            if (fact == null) Debug.Log(string.Format("fact {0} not found", factId));
            if (fact != null)
            {
                Debug.Log("fact is " + fact.Caption);
                //ShowTypes(fact.ClassName);
                res = GetInstance2(fact.ClassName, new object[] {fact, avatar}) as InteractiveUnit;
                units.Add(res);
            }
            Debug.Log(res);
            return res;
        }
    }
}
