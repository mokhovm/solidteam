﻿using System;
using System.Collections;
using System.Collections.Generic;
using Common.Units;
using PureMVC.Interfaces;
using PureMVC.Patterns;
using PureMVC.Unity;
using UnityEngine;
using Object = UnityEngine.Object;


namespace SampleGameNamespace
{
    /// <summary>
    /// Медиатор обслуживает зону сражения
    /// Должен создать:
    /// - медиатор игрока
    /// - медиатор интерфейса игры
    /// - медиаторы модальных форм
    /// - медиатор ввода 
    /// - медиаторы врагов
    /// - медиаторы предметов
    /// - медиатор уровня?
    /// </summary>
    public class MdBattleZone : Mediator
    {

        public new const string NAME = "MdBattleZone";

        // ссылка на прокси сцены
        private PrBattleZone _prBattleZone;
        private PrFactDict m_prFactDict;

        // ссылки на формы 
        private GameObject hero;
        private GameObject hud;
        private GameObject pauseForm;
        private GameObject settingsForm;
    
        public MdBattleZone() : base(NAME)
        {

        }

        public override void OnRegister()
        {
            Debug.Log("OnRegister " + NAME);
            _prBattleZone = Facade.RetrieveProxy(PrBattleZone.NAME) as PrBattleZone;
            m_prFactDict = Facade.RetrieveProxy(PrFactDict.NAME) as PrFactDict; 

            hero = Tools.FindObjectByName("Hero");
            pauseForm = Tools.FindObjectByName("FrmPause" + "(Clone)");
            settingsForm = Tools.FindObjectByName("SettingsForm" + "(Clone)");

            MdPlayerInput mdPlayerInput = UnityEngine.Object.FindObjectOfType<MdPlayerInput>();
            MdHud mdHud = UnityEngine.Object.FindObjectOfType<MdHud>();
            MdHero mdHero = UnityEngine.Object.FindObjectOfType<MdHero>();
            MdWeapon mdWeapon = UnityEngine.Object.FindObjectOfType<MdWeapon>();

            Transform canvas = Tools.FindObjectByName(MyResources.DEF_CANVAS_NAME).transform;
            // меню паузы
            pauseForm = Tools.instantiateObject(MyResources.FROM_PAUSE, canvas);
            pauseForm.SetActive(false);
            Debug.Log(MyResources.FROM_PAUSE + " created");

            // меню настроек
            settingsForm = Tools.instantiateObject(MyResources.FROM_SETTINGS, canvas);
            settingsForm.SetActive(false);
            Debug.Log(MyResources.FROM_SETTINGS + " created");


            //_prGame.sceneStart();
            Facade.RegisterMediator(new MdPause(pauseForm));
            Facade.RegisterMediator(new MdSettings(settingsForm));
            Facade.RegisterMediator(new MdSound());
            Facade.RegisterMediator(mdPlayerInput);
            Facade.RegisterMediator(mdHud);
            Facade.RegisterMediator(mdHero);
            Facade.RegisterMediator(mdWeapon);
            InitSceneObjects();
        }

        public override void OnRemove()
        {
            base.OnRemove();
            Debug.Log("OnRemove " + NAME);
            Facade.RemoveMediator(MdPause.NAME);
            Facade.RemoveMediator(MdSettings.NAME);
            Facade.RemoveMediator(MdSound.NAME);
            Facade.RemoveMediator(MdPlayerInput.NAME);
            Facade.RemoveMediator(MdHud.NAME);
            Facade.RemoveMediator(MdHero.NAME);
            Facade.RemoveMediator(MdWeapon.NAME);
            _prBattleZone = null;
            m_prFactDict = null;
        }

        private void InitSceneObjects()
        {
            var objectList = Resources.FindObjectsOfTypeAll(typeof(GameObject));
         
            foreach(var obj in objectList)
            {
                var go = obj as GameObject;
                if (go == null || !go.name.Contains("iu_")) continue;
                var words = go.name.Split('_');
                var objType = words[1];
                var objId = words[2];

                Debug.Log(string.Format("find objType {0} objId {1}", objType, objId));
                var unit = m_prFactDict.Facts.Find(x => x.Id.Equals(int.Parse(objId)));
                if (unit != null)
                {
                    Debug.Log(string.Format("Found {0}", unit.Caption));
                    InstantiateUnit(go, unit);
                }
            }
        }

        /// <summary>
        /// Создает юнит и активирует его
        /// </summary>
        /// <param name="fact"></param>
        /// <exception cref="NotImplementedException"></exception>
        private void InstantiateUnit(GameObject go, BaseFact fact)
        {
            /*  заметки по методу
                задачи, которые выполняет этот метод
                1. Удалить ненужный объект со сцены, а на его месте инстанцировать префаб
                2. Создать медиатор для управления интерактивныйм юнитом
                3. Создать прокси для хранения данных юнита
                4. Инициализировать и запустить работу кода, который обслуживает работу юнита
            */
            var sr = go.GetComponent<SpriteRenderer>();
            if (sr != null) Object.Destroy(sr);
            Tools.instantiateObject(fact.PrefabName, go.transform);
        }


        public override IList<string> ListNotificationInterests()
        {
            IList<string> notes = new System.Collections.Generic.List<string>();
            notes.Add(BzMessages.STATE_CHANGE);
            notes.Add(BzMessages.STATE_WAS_CHANGED);
            notes.Add(BzMessages.AVATAR_DEAD);
            return notes;
        }

        public override void HandleNotification(INotification note)
        {
            switch (note.Name)
            {
                case BzMessages.STATE_CHANGE:
                    _prBattleZone.setStateByMessage(note.Type);
                    break;
                case BzMessages.STATE_WAS_CHANGED:
                    if ((ZoneState)(int)note.Body == ZoneState.zsEnd)
                    {
                        Debug.Log(note.Type);
                        SendNotification(BaseMessages.NOTE_SWITCH_SCENE, null, BaseMessages.SCENE_MAIN_MENU);
                    }
                    break;
                case BzMessages.AVATAR_DEAD:
                    SendNotification(BaseMessages.NOTE_SWITCH_SCENE, null, BaseMessages.SCENE_MAIN_MENU);
                    break;


            }
        }
    }
}
