using System;
using System.Collections.Generic;
using Common.Units;
using PureMVC.Interfaces;
using PureMVC.Unity;
using UnityEngine;

namespace SampleGameNamespace
{
    public class MdLamp : MediatorBehavior
    {
        public new const string NAME = "MdLamp";

        // ид факта
        private int factId = 1;
        private PrBattleZone m_prBattleZone;
        private InteractiveUnit m_proxyUnit;
        private Animator m_animator;
        

        public MdLamp() : base(NAME)
        {
            
        }
        
        void Start()
        {
            Debug.Log("start lamp with " + factId);
            m_animator = GetComponent<Animator>();
            m_prBattleZone = Facade.RetrieveProxy(PrBattleZone.NAME) as PrBattleZone;
            if (m_prBattleZone == null) throw new Exception(PrBattleZone.NAME + " is not found!");
            if (m_proxyUnit == null)
                m_proxyUnit = m_prBattleZone.CreateUnit(factId, gameObject);
            Facade.RegisterMediator(this);
        }

        void Awake()
        {
            Debug.Log("awake lamp");
        }
        
        public override void OnRegister()
        {
            Debug.Log("register lamp");
            base.OnRegister();
            Debug.Log(NAME + " OnRegister");
            m_proxyUnit.StateMachine.ChangeState("init", null);
        }

        public override void OnRemove()
        {
            base.OnRemove();
            m_prBattleZone = null;
            // нужно перевести состояние юнита в free
            // m_proxyUnit.State =  
            Debug.Log(NAME + " OnRemove");
        }

        private void FixedUpdate()
        {
            if (m_proxyUnit != null)
            {
                //Debug.Log("try calculate");
                m_proxyUnit.calculatePhysics();
            }
        }

        private void Update()
        {
            if (m_proxyUnit != null)
            {
                m_proxyUnit.calcaulate();
            }
        }
        
        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.gameObject.CompareTag("Player"))
            {
                m_proxyUnit.changeLight();
            }
        }

        private void UpdateLight(bool isEnable)
        {
            if (m_animator)
            {
                Debug.Log("change light to " + isEnable);
                m_animator.SetBool("IsEnabled", isEnable);    
            }
        }
        
        public override IList<string> ListNotificationInterests()
        {
            IList<string> notes = new List<string>();
            notes.Add(BzMessages.LAMP_SWITCH);
            return notes;
        }

        public override void HandleNotification(INotification note)
        {
            switch (note.Name)
            {
                case BzMessages.LAMP_SWITCH:
                    UpdateLight((bool)note.Body);
                    break;
            }
        }        
    }
}