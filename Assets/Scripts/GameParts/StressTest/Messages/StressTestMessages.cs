namespace SampleGameNamespace
{
    public class StressTestMessages : BaseMessages
    {
        public const string CMD_STRESSTEST_SCENE_STARTUP = APP_PREFIX + "note/stresstest/startUp";
        public const string CMD_STRESSTEST_SCENE_SHUTDOWN = APP_PREFIX + "note/stresstest/shutdown";
        public const string NOTE_STRESSTEST_GASMAN_FLIP = APP_PREFIX + "note/stresstest/flip";
        public const string NOTE_STRESSTEST_GASMAN_SAY = APP_PREFIX + "note/stresstest/say";
        public const string NOTE_STRESSTEST_GASMAN_TOGGLE = APP_PREFIX + "note/stresstest/toggle";
    }
}