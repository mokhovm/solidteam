using PureMVC.Interfaces;
using PureMVC.Patterns;
using UnityEngine;

namespace SampleGameNamespace
{
    /*
     * Стартует сцену теста производительности
     */
    public class CmdStressTestStartup : SimpleCommand
    {
        public override void Execute(INotification notification)
        {
            Debug.Log("execute CmdStressTestStartup " + this.ToString());

            Facade.RegisterProxy(new PrStressTestScene());
            Facade.RegisterMediator(new MdStressTestScene());
        }
    }
}