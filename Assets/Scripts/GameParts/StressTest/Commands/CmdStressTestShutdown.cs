using PureMVC.Interfaces;
using PureMVC.Patterns;
using UnityEngine;

namespace SampleGameNamespace
{
    
    /// <summary>
    /// Команда заканчивает работать со сценой проверки производительности
    /// </summary>
    public class CmdStressTestShutdown : SimpleCommand
    {
        public override void Execute(INotification notification)
        {
            Debug.Log("execute CmdStressTestShutdown");
            Facade.RemoveMediator(MdStressTestScene.NAME);
            Facade.RemoveProxy(PrStressTestScene.NAME);
        }

    }
}