using System.Collections.Generic;
using PureMVC.Interfaces;
using PureMVC.Patterns;
using UnityEngine;

namespace SampleGameNamespace
{

    /// <summary>
    /// Медиатор управляет сценой нагрузочного тестирования.
    /// На сцене создаются объекты, перемещающиеся в разных направлениях и посылающие сообщения по шине данных
    /// </summary>
    public class MdStressTestScene : Mediator
    {
        public new const string NAME = "MdStressTestScene";

        private const string GASMAN_MEDIATOR_NAME = "gasman_";

        // количество объектов на экране  
        private int _mediatorQty = 300;
        

        //private GameObject _hero;

        // ссылка на прокси сцены
        private PrStressTestScene _prScene;

 
        public MdStressTestScene() : base(NAME, null)
        {
        }

        public override void OnRegister()
        {
            base.OnRegister();
            Debug.Log("OnRegister " + NAME);
            _prScene = Facade.RetrieveProxy(PrStressTestScene.NAME) as PrStressTestScene;
            
            
            
            
            //_hero = Tools.FindObjectByName("Gasman");
            //var mdGasman = UnityEngine.Object.FindObjectOfType<MdGasman>();
            for (int i = 0; i < _mediatorQty; i++)
            {
                var go = Tools.instantiateObject(MyResources.GAS_MAN);
                MdGasman m = go.AddComponent<MdGasman>();
                m.MediatorName = GASMAN_MEDIATOR_NAME + i.ToString();
                Facade.RegisterMediator(m);
            }
            Facade.RegisterMediator(new MdMsgCounter());
        }
        

        public override void OnRemove()
        {
            base.OnRemove();
            Debug.Log("OnRemove " + NAME);
            for (int i = 0; i < _mediatorQty; i++)
            {
                Facade.RemoveMediator(GASMAN_MEDIATOR_NAME + i.ToString());
            }
            Facade.RemoveMediator(MdMsgCounter.NAME);
        }

        public override IList<string> ListNotificationInterests()
        {
            IList<string> notes = new System.Collections.Generic.List<string>();
            //notes.Add(MenuMessages.NOTE_SETTINGS_LEVEL);
            return notes;
        }
        public override void HandleNotification(INotification note)
        {
            switch (note.Name)
            {
            }
        }
    }
}