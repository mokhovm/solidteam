using System.Collections.Generic;
using PureMVC.Interfaces;
using PureMVC.Unity;
using UnityEngine;

namespace SampleGameNamespace
{
    /// <summary>
    /// Газовщик для проверки нагрузки на шину сообщений
    /// </summary>
    public class MdGasman : MediatorBehavior
    {
        //public new const string NAME = "MdGasman";
        
        private bool _isSendMsg = true;
        private Gasman _gasman;
        private PrStressTestScene _prScene;


        public MdGasman() : base()
        {
        }

        public override void OnRegister()
        {
            base.OnRegister();
            _prScene = Facade.RetrieveProxy(PrStressTestScene.NAME) as PrStressTestScene;
            _gasman = new Gasman();
            _prScene.gasmanList.Add(_gasman);
            _gasman.init(this.gameObject);
            Debug.Log(NAME + " OnRegister");
        }

        public override void OnRemove()
        {
            base.OnRemove();
            _gasman = null;
            _prScene = null;
            Debug.Log(NAME + " OnRemove");
        }

        private void FixedUpdate()
        {
           if (_gasman != null) _gasman.calculatePhysics();
        }

        private void Update()
        {
           if (_gasman != null) _gasman.calcaulate();
            if (_isSendMsg) SendNotification(StressTestMessages.NOTE_STRESSTEST_GASMAN_SAY);
        }

        void flip()
        {
            Vector3 theScale = transform.localScale;
            theScale.x *= -1;
            transform.localScale = theScale;
        }

        private void OnCollisionEnter2D(Collision2D collision)
        {
            Debug.Log("OnCollisionEnter2D");
            _gasman.checkColision(collision);
        }


        public override IList<string> ListNotificationInterests()
        {
            IList<string> notes = new System.Collections.Generic.List<string>();
            notes.Add(StressTestMessages.NOTE_STRESSTEST_GASMAN_FLIP);
            notes.Add(StressTestMessages.NOTE_STRESSTEST_GASMAN_TOGGLE);
            return notes;
        }

        public override void HandleNotification(INotification note)
        {
            switch (note.Name)
            {
                case StressTestMessages.NOTE_STRESSTEST_GASMAN_FLIP:
                    if (note.Body == _gasman) flip();
                    break;
                case StressTestMessages.NOTE_STRESSTEST_GASMAN_TOGGLE:
                    _isSendMsg = !_isSendMsg;
                    break;
            }
        }
    }
}