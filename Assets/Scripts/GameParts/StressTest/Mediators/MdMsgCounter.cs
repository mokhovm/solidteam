using System.Collections.Generic;
using PureMVC.Interfaces;
using PureMVC.Patterns;
using UnityEngine;
using UnityEngine.UI;

namespace SampleGameNamespace
{
    /// <summary>
    /// Данный медиатор управляет на сцене интерфейсом с кнопкой старт/стоп и лейбом, отображающим количество отосланных
    /// сообщений 
    /// </summary>
    public class MdMsgCounter : Mediator
    {
        public new const string NAME = "MdMsgCounter";

        private int _qty;
        private UnityEngine.UI.Text _qtyText;
        private UnityEngine.UI.Button _btn;
        
        public MdMsgCounter() : base(NAME, null)
        {
        }

        public override void OnRegister()
        {
            base.OnRegister();
            GameObject go = Tools.FindObjectByName("TextQty");
            if (go)
                _qtyText = go.GetComponent<Text>();
            
            go = Tools.FindObjectByName("Button");
            if (go)
                _btn = go.GetComponent<Button>();

            _btn.onClick.AddListener(OnBtnClick);
                
            
            Debug.Log("OnRegister " + NAME);
        }
        

        public override void OnRemove()
        {
            base.OnRemove();
            _btn.onClick.RemoveListener(OnBtnClick);
            Debug.Log("OnRemove " + NAME);
        }

        private void OnBtnClick()
        {
            SendNotification(StressTestMessages.NOTE_STRESSTEST_GASMAN_TOGGLE);
        }
        
        private void updateQty()
        {
            _qty++;
            _qtyText.text = _qty.ToString();

        }        

        public override IList<string> ListNotificationInterests()
        {
            IList<string> notes = new System.Collections.Generic.List<string>();
            notes.Add(StressTestMessages.NOTE_STRESSTEST_GASMAN_SAY);
            return notes;
        }
        public override void HandleNotification(INotification note)
        {
            switch (note.Name)
            {
                case StressTestMessages.NOTE_STRESSTEST_GASMAN_SAY:
                    updateQty();
                    break;
            }
        }

        
    }
}