using UnityEngine;

namespace SampleGameNamespace
{
    /// <summary>
    /// Газовщик 
    /// </summary>
    public class Gasman
    {
        // ссылка на аватар героя
        public GameObject avatar;
        
        // направление туловища аватара
        public AvararDir direction = AvararDir.adRight;    
        // максимальная скорость аватара
        public float maxSpeed;
        
        public void init(GameObject avatar)
        {
            this.avatar = avatar;
            maxSpeed = Random.Range(0.01f, 0.1f);
        }
        
        
        public Vector3 getSpeed()
        {
            return (direction == AvararDir.adLeft ? Vector2.left : Vector2.right) * maxSpeed;
        }


            /// <summary>
        /// Осуществляем управление аватаром
        /// Этот метод должен вызываться из Update
        /// </summary>
        public void calcaulate()
        {
            if (!avatar) return;
            avatar.transform.position += getSpeed();
        }
        

        /// <summary>
        /// Расчитываем поведение персонажа в физическом мире. 
        /// Этот метод должен вызываться из FixedUpdate
        /// </summary>
        public void calculatePhysics()
        {

        }

        /// <summary>
        /// Разворачивает аватар игрока в противоположную сторону
        /// </summary>
        public void doFlip()
        {
            direction = direction != AvararDir.adLeft ? AvararDir.adLeft : AvararDir.adRight;
            MyGameFacade.Instance.SendNotification(StressTestMessages.NOTE_STRESSTEST_GASMAN_FLIP, this);
        }

        public void checkColision(Collision2D collision)
        {
            doFlip();
            
        }
    }
}