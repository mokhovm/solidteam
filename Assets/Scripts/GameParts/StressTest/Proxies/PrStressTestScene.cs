//using Boo.Lang;

using System.Collections.Generic;
using PureMVC.Patterns;
using UnityEngine;

namespace SampleGameNamespace
{
    public class PrStressTestScene : Proxy
    {
        public new const string NAME = "PrStressTestScene";

        //public Gasman gasman;
        public List<Gasman> gasmanList;

        public PrStressTestScene() : base (NAME, null)
        {
            
        }

        public override void OnRegister()
        {
            base.OnRegister();
            gasmanList = new List<Gasman>();
            //gasman =  new Gasman();
            Debug.Log("OnRegister " + NAME);
        }

        public override void OnRemove()
        {
            base.OnRemove();
            //gasman = null;
            gasmanList.Clear();
            gasmanList = null;
            Debug.Log("OnRemove " + NAME);
        }
        
         
    }
}