﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SampleGameNamespace
{
    public class AudioSettings
    {
        public float masterVolume = 0;
        public float playerVolume = 0;
        public float ambientVolume = -30;
        public float musicVolume = -15;
        public bool isUseAudio = true;
        public bool isUseMusic = true;
    }
}
