﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Common.Units;
using Common.Units.fsm;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using UnityEngine;
//using UnityEngine.Experimental.UIElements;


/// <summary>
/// Данный юнит предназначен для теста сериализации класса Fact
/// </summary>
public class TestJsonScript : MonoBehaviour
{


	private Vector3 v3;
	private Fact fact;
	private string json;
	private UnitStateMachine machine = new UnitStateMachine(null);
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}


	public void TestJsonSerializeV3()
	{
		Debug.Log("Serialize v3");
		v3  = Vector3.back;
		json = SerializeObject.Serialize(v3);
		Debug.Log(json);
	}
	
	public void TestJsonDeserializeV3()
	{
		Debug.Log("Deserialize v3");
		v3 = SerializeObject.Deserialize<Vector3>(json);
		Debug.Log(v3);
	}

	public void TestJsonSerializeFact()
	{
		Debug.Log("Serialize Fact");
		fact  = new Fact();
		json = SerializeObject.Serialize(fact);
		Debug.Log(json);
	}
	
	public void TestJsonDeserializeFact()
	{
		Debug.Log("Deserialize Fact");
		fact = SerializeObject.Deserialize<Fact>(json);
		Debug.Log(fact);
	}
	
	public void TestJsonSerializeMachine()
	{
		Debug.Log("Serialize machine");
		/*
		machine = new UnitStateMachine(null);
		var state1 = new UnitState("test", "entering", "exiting", "changed");
		var state2 = new UnitState("some", "entering2", "exiting2", "changed2");
		state1.DefineTrans("action1", "state2");
		state1.DefineTrans("action2", "state1");
		state2.DefineTrans("action3", "state1");
		machine.RegisterState(state1);
		machine.RegisterState(state2);
		json = JsonConvert.SerializeObject(machine);  
		Debug.Log(json);
		*/
		
		
		Debug.Log("Deserialize JSON");
		json = System.IO.File.ReadAllText(@"Assets\Scripts\GameParts\BattleZone\Proxies\states.json");

	}


	public void TestJsonDeserializeMachine()
	{
		Debug.Log("Deserialize machine");
		// не восстанавливает толком
		//machine = JsonConvert.DeserializeObject<UnitStateMachine>(json);
		
		JObject fsm = JObject.Parse(json);

		// get JSON result objects into a list
		IList<JToken> states = fsm["states"].Children().ToList();
		Debug.Log("found " + states.Count + " states");

		// serialize JSON results into .NET objects
		foreach (JToken value in states)
		{
			Debug.Log("get new json state " + value.ToString());
			// JToken.ToObject is a helper method that uses JsonSerializer internally
			UnitState newState = value.ToObject<UnitState>();
			Debug.Log("newState " + newState.Name + " deserialized complete");
			IList<JToken> transitions = value["Transitions"].Children().ToList();
			foreach (JToken tra in transitions)
			{
				newState.DefineTrans(tra["action"].ToString(), tra["target"].ToString());
			}
			Debug.Log(newState.Name + " transitions qty=" + newState.GetTransitionQty() );
			machine.RegisterState(newState);
		}
		Debug.Log(machine);
	}

}
