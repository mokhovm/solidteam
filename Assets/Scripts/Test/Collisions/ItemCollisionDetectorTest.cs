﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
///  Скрипт для интерактивных предметов, которые тоже могут чекать различные типы коллизий
/// </summary>
public class ItemCollisionDetectorTest : MonoBehaviour {
    
    private void OnTriggerEnter2D(Collider2D other)
    {
        Debug.Log("Interactive item trigger enter");
    }
    
    private void OnTriggerStay2D(Collider2D other)
    {
        Debug.Log("Interactive item trigger stay");
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        Debug.Log("Interactive item trigger exit");
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        Debug.Log("Interactive item collision enter");
    }

    private void OnCollisionExit2D(Collision2D other)
    {
        Debug.Log("Interactive item collision exit");
    }

    private void OnCollisionStay2D(Collision2D other)
    {
        Debug.Log("Interactive item collision stay");
    }
}
