﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SampleGameNamespace
{
    /*
     * Статический класс для описания ссылок на ресурсы, сиспользуемые в игре
     */
    internal static class MyResources
    {
        public const string DEF_CANVAS_NAME = "Canvas";

        public const string PLAYER_INPUT = "prefabs/components/PlayerInput";
        public const string PLAYER_HUD = "prefabs/components/Hud";
        public const string FROM_PAUSE = "prefabs/components/forms/FrmPause";
        public const string FROM_SETTINGS = "prefabs/components/forms/FrmSettings";
        public const string PLAYER_HERO = "prefabs/components/Hero";
        public const string GAS_MAN = "prefabs/Gasman";
    }
}
